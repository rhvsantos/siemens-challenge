#!/usr/bin/env bash
#
# By: Rafael Santos (04/01/2023)
# Find duplicated fake IP addresses and keep just the first one. All other duplicated IP addresses must be removed
# Without using an Array
#
#
export IFS=$'\n'

OUTPUT_FILE="/tmp/output_1.txt"

# Check the argument
[ $# -ne 1 ] && { echo "Usage:
	$0 <SERVER IP LIST FILE>" ; exit 1; }

# Check if the file exists
[ ! -e $1 ] && { echo "File $1 does not exist!"; exit 1; }

# Erase Output file
echo > $OUTPUT_FILE

# Loop for each line
cat $1 | while read line
do
	# Get Ip Address Using Bash Parameter Expansion (For this example, the Ip Address is on the first position)
	if grep -qE '^[[:digit:]]' <<< "$line"
	then
		_ip=${line%% *}
	else
		_ip="$line"
	fi

	# If the $_ip isn't present on the OUTPUT_FILE, write the line into it.
	if ! grep -q $_ip $OUTPUT_FILE || grep -Eq "^#" <<< "$_ip"
	then
		echo "$line" >> $OUTPUT_FILE
	fi
done

echo "it's done, now you can check the Output file: $OUTPUT_FILE"
