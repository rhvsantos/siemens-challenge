#!/usr/bin/python
#from icecream import ic
import os.path
import sys

ORIGINAL_FILE = "/tmp/output_1.txt"
OUTPUT_FILE = "/tmp/output_2.txt"

if not os.path.exists(ORIGINAL_FILE) or not os.path.isfile(ORIGINAL_FILE):
    print(f'The file {ORIGINAL_FILE} does not exist or is not a file!')
    sys.exit(1)

with open(OUTPUT_FILE, 'w') as output_file:
    with open(ORIGINAL_FILE, 'r', newline='') as original_file:
        
        reader = original_file.readlines()

        for line in reader:

            # If it is a commented line
            if line.startswith('#'):
                output_file.write(line)

            # Write the first two columns into Output file
            if len(line) > 3 and not line.startswith('#'):
                first_column, *rest = line.split()
                
                # Define the new line
                full_line = f'{first_column:<25s} {"   ".join(rest)}\n'
                # Write into Output file
                output_file.write(full_line)