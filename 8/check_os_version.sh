#!/usr/bin/env bash
#
#
# By: Rafael Santos (04/01/2023)
# Check OS Version from multiple Linux Servers
# This script reads a file that contains all servers IP Address
#
#

SSH_USER='myuser'
OUTPUT_FILE='servers_version.txt'

# Check the argument
[ $# -ne 1 ] && { echo "Usage:
	$0 <SERVER IP LIST FILE>" ; exit 1; }

# Check if the file exists
[ ! -e $1 ] && { echo "File $1 does not exist!"; exit 1; }

# Erase Output File
echo > $OUTPUT_FILE

cat $1 | while read line
do
	command=$(ssh -T $SSH_USER@$line "hostname -f; echo ; uname -srvmo" < /dev/null 2> /dev/null)
	
	_date=`date +"%H:%M:%S %m/%d/%Y"`
	_hostname=`echo $command | awk '{print$1}'`
	_os_version=`echo ${command#$_hostname} | tr -d '\n'`

	echo -e "$_date\t$_hostname\t$line\t$_os_version\n" >> $OUTPUT_FILE
done
